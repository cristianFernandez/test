import React, { useState } from "react";
import classNames from "classnames";
import styles from "./ArticleContainer.module.scss";
import Selector from "./Selector/SelectorComponent";
import ImageViewer from "../../ImageView/ImageView";
import SearchModal from "./SearchModal/SearchModal";

interface ArticleContainerProps {
  /** Name of classComponent to merge */
  ClassName?: string;
}

/**
 * ArticleContainer Component.
 */
const ArticleContainerComponent: React.FC<ArticleContainerProps> = props => {
  const [contentUrl, setContentUrl] = useState("");
  const [showSearch, setShowSearch] = useState(false);

  return (
    <div className={classNames(props.ClassName, styles.container)}>
      {showSearch && (
        <SearchModal
          Show={showSearch}
          OnHide={() => setShowSearch(false)}
          OnContentSelected={(value: string) => {
            setShowSearch(false);
            setContentUrl(value);
          }}
        />
      )}
      {contentUrl ? (
        <ImageViewer
          Source={contentUrl}
          OnEditableClick={() => setShowSearch(true)}
        />
      ) : (
        <Selector
          OnSearchClick={() => {
            setShowSearch(true);
          }}
        />
      )}
    </div>
  );
};

export default ArticleContainerComponent;
