import React from "react";
import classNames from "classnames";
import styles from "./Input.module.scss";
import Icon, { IconType } from "../Icon/Icon";

interface InputProps {
  /** Name of classComponent to merge */
  ClassName?: string;

  /** Type of input */
  Type: string;

  /** Callback with the new values */
  OnChange: Function;

  /** Text value */
  Value?: string;

  /** Icontype who will appears with the input */
  IconType?: IconType;

  /** Callback that will be executed when the user clicks on the icon */
  OnIconClick?: Function;
}

/**
 * Input Component.
 */
const InputComponent: React.FC<InputProps> = props => {
  return (
    <div className={classNames(props.ClassName, styles.container)}>
      <input
        type={props.Type}
        onChange={event => props.OnChange(event.target.value)}
        value={props.Value}
      />
      {props.IconType && (
        <div
          className={styles.iconContent}
          onClick={() => props.OnIconClick && props.OnIconClick()}
        >
          <Icon Type={props.IconType} ClassName={styles.icon} />
        </div>
      )}
    </div>
  );
};

InputComponent.defaultProps = {
  Type: "text"
};

export default InputComponent;
