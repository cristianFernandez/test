import React from "react";
import Button from "../../../../Button/Button";
import classNames from "classnames";
import ImageView from "../../../../ImageView/ImageView";
import styles from "./SelectorImageView.module.scss";
import Icon, { IconType } from "../../../../Icon/Icon";

export enum SelectorImageViewType {
  IMAGE = "image",
  VIDEO = "video"
}

interface SelectorImageViewProps {
  /** Function to execute once a click event happens */
  OnSelect?: Function;

  /** Name of classComponent to merge */
  ClassName?: string;

  /** Title of the image */
  Title: string;

  /** License of the image */
  License: string;

  /** Height of the image (as a representation purpouse) */
  Height: string;

  /** License of the image (as a representation purpouse)*/
  Width: string;

  /** URL of the resource */
  Url: string;

  /** Resource Type */
  Type: SelectorImageViewType;
}

/**
 * SelectorImageView Component.
 */
const SelectorImageViewComponent: React.FC<SelectorImageViewProps> = props => {
  return (
    <div className={classNames(styles.content, props.ClassName)}>
      <div className={styles.facade}>
        <div className={styles.contentFacade}>
          <div>{props.Title}</div>
          <div>
            {props.Height}x{props.Width}
          </div>
          <div>{props.License}</div>
          <Button
            Text="Insert"
            Variant="primary"
            OnClick={() => props.OnSelect && props.OnSelect()}
          />
        </div>
      </div>
      <div className={styles.imageContent}>
        {props.Type === SelectorImageViewType.VIDEO && (
          <Icon Type={IconType.Play} ClassName={styles.icon} />
        )}
        <ImageView ClassName={styles.image} Source={props.Url} />
      </div>
    </div>
  );
};

export default SelectorImageViewComponent;
