import mediaMocks from "../../../../mocks/medias.json";

export const getMedia = (
  page: number = -1,
  site: string = "",
  authorId: number = -1,
  type: string = ""
) => {
  let mediaReturn = mediaMocks;

  //Page property is ignored, the method will return always the same.

  if (site !== "" || authorId !== -1 || type !== "") {
    mediaReturn = mediaReturn.filter(entry => {
      let resultSite = true;
      let resultAuthorId = true;
      let resultType = true;

      if (site !== "") {
        resultSite = !!entry.site.toLowerCase().match(site.toLowerCase());
      }

      if (authorId !== -1) {
        resultAuthorId = entry.author.id === authorId;
      }

      if (type !== "") {
        resultType = !!entry.type.toLowerCase().match(type.toLowerCase());
      }

      return resultSite && resultAuthorId && resultType;
    });
  }

  return mediaReturn;
};
