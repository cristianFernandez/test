import React from "react";
import classNames from "classnames";
import styles from "./Selector.module.scss";
import { default as Icon, IconType } from "../../../Icon/Icon";

interface SelectorProps {
  /** Name of classComponent to merge */
  ClassName?: string;

  /** Function for Image selector */
  OnImageClick?: Function;

  /** Function for Play selector */
  OnPlayClick?: Function;

  /** Function for Galery selector */
  OnGaleryClick?: Function;

  /** Function for Search selector */
  OnSearchClick?: Function;
}

/**
 * Selector Component.
 */
const SelectorComponent: React.FC<SelectorProps> = props => {
  return (
    <div className={classNames(props.ClassName, styles.container)}>
      <div
        className={styles.topLeft}
        onClick={() => props.OnImageClick && props.OnImageClick()}
      >
        <Icon Type={IconType.Image} ClassName={styles.topLeft} />
      </div>
      <div
        className={styles.topRight}
        onClick={() => props.OnPlayClick && props.OnPlayClick()}
      >
        <Icon Type={IconType.Play} ClassName={styles.topRight} />
      </div>
      <div
        className={styles.bottomLeft}
        onClick={() => props.OnGaleryClick && props.OnGaleryClick()}
      >
        <Icon Type={IconType.Galery} ClassName={styles.bottomLeft} />
      </div>
      <div
        className={styles.bottomRight}
        onClick={() => props.OnSearchClick && props.OnSearchClick()}
      >
        <Icon Type={IconType.Search} ClassName={styles.bottomRight} />
      </div>
    </div>
  );
};

export default SelectorComponent;
