import IFetch from "./model/IFetch";
import FetchMock from "./model/mocks/FetchMock";

export default class FetchFactory {
  static ENDPOINTS = {
    MEDIA: "media"
  };

  static getFecher(): IFetch {
    const isMocked = !!process.env.REACT_APP_MOCKED;
    const url = process.env.REACT_APP_HOST ? process.env.REACT_APP_HOST : "";

    //TODO: Add no mocked Fetch implementation once API Server is defined
    if (!isMocked) {
      throw new Error("Method not implemented.");
    }

    return new FetchMock(url);
  }
}
