import React from "react";
import Modal from "react-bootstrap/Modal";

interface ModalProps {
  /** Control if the modal should be visible*/
  Show: boolean;

  /** Function to execute on hide event */
  OnHide: Function;
}

/**
 * Modal Component.
 */
const ModalComponent: React.FC<ModalProps> = props => {
  return (
    <Modal size="xl" show={props.Show} onHide={() => props.OnHide()} centered>
      <Modal.Body>{props.children}</Modal.Body>
    </Modal>
  );
};

export default ModalComponent;
