import React from "react";
import classNames from "classnames";
import styles from "./Icon.module.scss";

export enum IconType {
  Hamburger,
  Image,
  Play,
  Galery,
  Search,
  Edit,
  Close
}

interface IconProps {
  /** Type of icon. See definition file*/
  Type: IconType;

  /** Name of classComponent to merge */
  ClassName?: string;

  /** Function to call once a click events happens */
  OnClick?: Function;
}

const parseIconType = (type: IconType): string => {
  let className = "fa ";
  switch (type) {
    case IconType.Hamburger:
      return className + "fa-bars";

    case IconType.Image:
      return className + "fa-image";

    case IconType.Play:
      return className + "fa-play-circle";

    case IconType.Galery:
      return className + "fa-delicious";

    case IconType.Search:
      return className + "fa-search";

    case IconType.Edit:
      return className + "fa-pencil";

    case IconType.Close:
      return className + "fa-times";

    default:
      return "";
  }
};

/**
 * Icon Component.
 */
const IconComponent: React.FC<IconProps> = props => {
  return (
    <i
      onClick={() => props.OnClick && props.OnClick()}
      className={classNames(
        styles.icon,
        props.ClassName,
        parseIconType(props.Type)
      )}
    />
  );
};

export default IconComponent;
