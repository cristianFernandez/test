import React from "react";
import "react-toggle/style.css";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import _ from "lodash";

export interface DropdownItem {
  Title: string;
  OnClickAction: Function;
}

interface DropdownProps {
  /** Default value to show*/
  Title: string;

  /** Array of values */
  Values: Array<DropdownItem>;
}

/**
 * Dropdown Component.
 */
const DropdownComponent: React.FC<DropdownProps> = props => {
  return (
    <DropdownButton
      title={props.Title}
      variant="outline-primary"
      id={_.uniqueId(`Dropdown-${props.Title}`)}
    >
      {props.Values.map((value, index) => (
        <Dropdown.Item
          key={index}
          eventKey={`${index}-asd`}
          onClick={() => value.OnClickAction()}
        >
          {value.Title}
        </Dropdown.Item>
      ))}
    </DropdownButton>
  );
};

export default DropdownComponent;
