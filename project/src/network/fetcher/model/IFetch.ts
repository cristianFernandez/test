export default interface IFetch {
  get(parcialUrl: string, parameters: any): Promise<any>;
  post(parcialUrl: string, parameters: any): Promise<any>;
}
