Input examples:

- Text:

```js
class InputExample extends React.PureComponent {
  constructor() {
    this.state = { value: "" };
  }

  render() {
    return (
      <Input
        OnChange={value => {
          console.log(value);
          this.setState({ value });
        }}
        Value={this.state.value}
      />
    );
  }
}

<InputExample />;
```

- Password:

```js
class InputExample extends React.PureComponent {
  constructor() {
    this.state = { value: "" };
  }

  render() {
    return (
      <Input
        Type="password"
        OnChange={value => {
          console.log(value);
          this.setState({ value });
        }}
        Value={this.state.value}
      />
    );
  }
}

<InputExample />;
```
