import React from "react";
import "./App.scss";
import Button from "./components/Button/Button";
import { default as Icon, IconType } from "./components/Icon/Icon";
import Link from "./components/Link/Link";

import ArticleContainer from "./components/Article/Container/ArticleContainer";

import Dante from "dante2";

const App: React.FC = () => {
  return (
    <div className="content">
      <Icon ClassName="hamburger" Type={IconType.Hamburger} />
      <Button ClassName="save" Text="GUARDAR" />
      <Link ClassName="preview" Text="Vista Previa" />
      <h1 className="title">Escribe aquí el titular de tu artículo</h1>
      <ArticleContainer ClassName="article" />
      <div className="editor">
        <Dante/>
      </div>
      
    </div>
  );
};

export default App;
