import React, { Fragment } from "react";
import classNames from "classnames";
import styles from "./ImageView.module.scss";
import Icon, { IconType } from "../Icon/Icon";

interface ImageViewProps {
  /** Name of classComponent to merge */
  ClassName?: string;

  /** Source Image */
  Source: string;

  /** Alternative Text if the image is not available */
  Alternative?: string;

  /** Function for Search selector */
  OnModifyClick?: Function;

  /** Editable function */
  OnEditableClick?: Function;

  /** On Click Event */
  OnClick?: Function;
}

/**
 * ImageView Component.
 */
const ImageViewComponent: React.FC<ImageViewProps> = props => {
  return (
    <Fragment>
      <img
        onClick={() => props.OnClick && props.OnClick()}
        className={classNames(props.ClassName, styles.image)}
        src={props.Source}
        alt={props.Alternative}
      />
      {props.OnEditableClick && (
        <div
          className={classNames(props.ClassName, styles.edit)}
          onClick={() => props.OnEditableClick && props.OnEditableClick()}
        >
          <Icon Type={IconType.Edit} />
        </div>
      )}
    </Fragment>
  );
};

export default ImageViewComponent;
