import React from "react";
import Modal from "../../../Modal/Modal";
import Icon, { IconType } from "../../../Icon/Icon";
import styles from "./SearchModal.module.scss";
import Input from "../../../Input/Input";
import InfiniteScroll from "react-infinite-scroll-component";
import FetchFactory from "../../../../network/fetcher/FetchFactory";
import SelectorImageView, {
  SelectorImageViewType
} from "./SelectorImageView/SelectorImageView";
import ToggleButton from "../../../ToggleButton/ToggleButton";
import Dropdown, { DropdownItem } from "../../../Dropdown/Dropdown";

const AUTHORMOCK: number = 1000; //TODO: Delte mock authorId Javier

enum MediaType {
  ALL = "Todos",
  IMAGE = "image",
  VIDEO = "video"
}

enum SiteType {
  ALL = "Todas",
  AUTOBILD = "autobild",
  GOOGLE = "google"
}

interface SearchModalProps {
  /** Control if the modal should be visible*/
  Show: boolean;

  /** Function to execute on hide event */
  OnHide: Function;

  /** Callback to pass a selected resource */
  OnContentSelected: Function;
}

interface SearchModalState {
  searchValue: string;
  currentPage: number;
  siteFilter: SiteType;
  authorId: number;
  typeFilter: MediaType;
  data: Array<any>;
}

/**
 * SearchModal Component.
 */
class SearchModalComponent extends React.Component<
  SearchModalProps,
  SearchModalState
> {
  constructor(props: SearchModalProps) {
    super(props);

    this.state = {
      searchValue: "",
      currentPage: 0,
      siteFilter: SiteType.ALL,
      authorId: -1,
      typeFilter: MediaType.ALL,
      data: []
    };
  }

  onFilterChange: Function = () => {
    this.setState({ currentPage: 0, data: [] }, () => this.fetchData());
  };

  changeAuthorIdFilter: Function = () => {
    this.setState(
      {
        authorId: this.state.authorId === -1 ? AUTHORMOCK : -1
      },
      () => this.onFilterChange()
    );
  };

  setWebFilter: Function = (value: SiteType) =>
    this.setState({ siteFilter: value }, () => this.onFilterChange());

  setMediaFilter: Function = (value: MediaType) =>
    this.setState({ typeFilter: value }, () => this.onFilterChange());

  setSearchValue: Function = (value: string) =>
    this.setState({ searchValue: value });

  fetchData: () => void = async () => {
    const fetch = FetchFactory.getFecher();

    const { currentPage, siteFilter, authorId, typeFilter } = this.state;

    const params = {
      page: currentPage,
      site: siteFilter === SiteType.ALL ? "" : siteFilter.toString(),
      authorId,
      type: typeFilter === MediaType.ALL ? "" : typeFilter.toString()
    };

    const data = await fetch.get(FetchFactory.ENDPOINTS.MEDIA, params);

    const arrayData: Array<any> = new Array(...data);

    const newData: Array<any> = new Array(...this.state.data);
    newData.push(...arrayData);

    this.setState({ data: newData });
  };

  next: () => void = () => {
    this.setState({ currentPage: this.state.currentPage + 1 });
    this.fetchData();
  };

  componentDidMount() {
    this.fetchData();
  }

  render() {
    const { searchValue, data, siteFilter, typeFilter } = this.state;
    const { Show, OnHide, OnContentSelected } = this.props;

    return (
      <Modal Show={Show} OnHide={() => OnHide()}>
        <div className={styles.container}>
          <Icon
            ClassName={styles.exit}
            Type={IconType.Close}
            OnClick={() => OnHide()}
          />
          <div className={styles.owner}>
            <span>SOLO LO MIO</span>
            <ToggleButton
              DefatulValue={false}
              OnChange={() => this.changeAuthorIdFilter()}
            />
          </div>
          <div className={styles.webs}>
            <span>WEBS</span>
            <Dropdown
              Title={siteFilter}
              Values={[
                {
                  Title: SiteType.AUTOBILD,
                  OnClickAction: () => this.setWebFilter(SiteType.AUTOBILD)
                } as DropdownItem,
                {
                  Title: SiteType.GOOGLE,
                  OnClickAction: () => this.setWebFilter(SiteType.GOOGLE)
                } as DropdownItem,
                {
                  Title: SiteType.ALL,
                  OnClickAction: () => this.setWebFilter(SiteType.ALL)
                } as DropdownItem
              ]}
            />
          </div>
          <div className={styles.types}>
            <span>TIPOS</span>
            <Dropdown
              Title={typeFilter}
              Values={[
                {
                  Title: MediaType.IMAGE,
                  OnClickAction: () => this.setMediaFilter(MediaType.IMAGE)
                } as DropdownItem,
                {
                  Title: MediaType.VIDEO,
                  OnClickAction: () => this.setMediaFilter(MediaType.VIDEO)
                } as DropdownItem,
                {
                  Title: MediaType.ALL,
                  OnClickAction: () => this.setMediaFilter(MediaType.ALL)
                } as DropdownItem
              ]}
            />
          </div>
          <Input
            ClassName={styles.search}
            Type="text"
            IconType={IconType.Search}
            OnChange={this.setSearchValue}
            Value={searchValue}
          />
          <div className={styles.content}>
            <InfiniteScroll
              dataLength={data.length}
              next={this.next}
              loader={<h4>Loading...</h4>}
              hasMore={true}
              height={400}
            >
              <div className={styles.contentFlex}>
                {data.map((element, index) => {
                  return (
                    <SelectorImageView
                      ClassName={styles.image}
                      Type={
                        element.type === "video"
                          ? SelectorImageViewType.VIDEO
                          : SelectorImageViewType.IMAGE
                      }
                      Title={element.name}
                      License={element.license}
                      Height={
                        element.type === "video"
                          ? element.thumbnail.height
                          : element.height
                      }
                      Width={
                        element.type === "video"
                          ? element.thumbnail.width
                          : element.width
                      }
                      OnSelect={() =>
                        OnContentSelected(
                          element.type === "video"
                            ? element.thumbnail.url
                            : element.url
                        )
                      }
                      Url={
                        element.type === "video"
                          ? element.thumbnail.url
                          : element.url
                      }
                      key={index}
                    />
                  );
                })}
              </div>
            </InfiniteScroll>
          </div>
        </div>
      </Modal>
    );
  }
}

export default SearchModalComponent;
