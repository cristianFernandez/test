import IFetch from "../IFetch";
import { mountURL } from "../../../Url";
import FetchFactory from "../../FetchFactory";
import { getMedia } from "./MediaMocks";

export default class FetchMock implements IFetch {
  url: string;

  constructor(url: string) {
    this.url = url;
  }

  get(parcialUrl: string, parameters: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = mountURL(this.url, parcialUrl);

      if (url.includes(FetchFactory.ENDPOINTS.MEDIA)) {
        const page = parameters.page || -1;
        const site = parameters.site || "";
        const authorId = parameters.authorId || -1;
        const type = parameters.type || "";

        resolve(getMedia(page, site, authorId, type));
      }

      reject("Method not implemented.");
    });
  }

  post(urparcialUrll: string, parameters: Object): Promise<any> {
    throw new Error("Method not implemented.");
  }
}
