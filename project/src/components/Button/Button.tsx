import React from "react";
import Button from "react-bootstrap/Button";
import classNames from "classnames";

interface ButtonProps {
  /** Text value to show*/
  Text: string;

  /** Function to execute once a click event happens */
  OnClick?: Function;

  /** Name of classComponent to merge */
  ClassName?: string;

  /** Variant */
  Variant?:
    | "primary"
    | "secondary"
    | "success"
    | "danger"
    | "warning"
    | "info"
    | "dark"
    | "light"
    | "link"
    | "outline-primary"
    | "outline-secondary"
    | "outline-success"
    | "outline-danger"
    | "outline-warning"
    | "outline-info"
    | "outline-dark"
    | "outline-light";
}

/**
 * Button Component.
 */
const ButtonComponent: React.FC<ButtonProps> = props => {
  return (
    <Button
      className={classNames(props.ClassName)}
      variant={props.Variant ? props.Variant : "outline-primary"}
      onClick={() => props.OnClick && props.OnClick()}
    >
      {props.Text}
    </Button>
  );
};

ButtonComponent.defaultProps = {
  OnClick: () => {}
};

export default ButtonComponent;
