export const mountURL = (baseUrl: string, parcialURL: string): string => {
  let urlMounted = `${baseUrl}/${parcialURL}`;

  //TODO: Improve method in order to be sure about the correct shape of the URL.
  return urlMounted.replace("//", "/");
};
