import React from "react";
import "react-toggle/style.css";
import Toggle from "react-toggle";
interface ToggleButtonProps {
  /** Default value to show*/
  DefatulValue: boolean;

  /** Function to execute once a click event happens */
  OnChange?: Function;
}

/**
 * ToggleButton Component.
 */
const ToggleButtonComponent: React.FC<ToggleButtonProps> = props => {
  return (
    <Toggle
      defaultChecked={props.DefatulValue}
      icons={false}
      onChange={() => props.OnChange && props.OnChange()}
    />
  );
};

export default ToggleButtonComponent;
