import React from "react";
import classNames from "classnames";

import Button from "react-bootstrap/Button";

interface LinkProps {
  /** Name of classComponent to merge */
  ClassName?: string;

  /** Text value to show*/
  Text: string;

  /** Function to execute once a click event happens */
  OnClick?: Function;
}
/**
 * Link Component.
 */
const LinkComponent: React.FC<LinkProps> = props => {
  return (
    <Button className={classNames(props.ClassName)} variant="link">
      {props.Text}
    </Button>
  );
};

export default LinkComponent;
